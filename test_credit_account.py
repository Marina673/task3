from bankaccountimpl import BankAccountImpl
from creditaccount import CreditAccount


class TestCreditAccount(object):
    def test_get_balance(self):
        openingBalance = 20
        parent = BankAccountImpl(openingBalance)
        subject = CreditAccount(parent)

        result = subject.get_balance()

        assert result == openingBalance


    def test_credit(self):
        openingBalance = 20
        credit_val = 150
        parent = BankAccountImpl(openingBalance)
        subject = CreditAccount(parent)

        subject.credit(credit_val)
        result = subject.get_balance()

        assert result == openingBalance + credit_val


    def test_debit(self):
        openingBalance = 120
        debit_val = 70
        parent = BankAccountImpl(openingBalance)
        subject = CreditAccount(parent)

        subject.debit(debit_val)
        result = subject.get_balance()

        assert result == openingBalance - debit_val



if __name__ == '__main__':
    test = TestCreditAccount()

    test.test_get_balance()
    test.test_credit()
    test.test_debit()
